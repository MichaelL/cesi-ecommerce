var products = [];
var productListHtml = [];
var categoryListHtml = [];
var carouselHtml = [];
var priceTable = [];
var listProductId = [];

var cartList = [];
var cartListHtml = [];

// récupére la liste de produit en API
function getProducts() {
  fetch("https://fakestoreapi.com/products")
    .then((res) => res.json())
    .then((json) => {
      products = json;
      generateProductsCard(products);
    });
}

// génére la liste de card produit en HTML
function generateProductsCard(products) {
  productListHtml = [];
  products.forEach((product, index) => {
    generateProductCategory(product);
    productListHtml.push(
        "<div class='card'>" +
        "<a href='product#" + product.id + "'>" +
        "<img class='card-img-top' style='width:100%;height:13rem;object-fit:cover;'" +
        " src='" + product.image + "' alt='" + product.title + "'>" +
        "</a>" +
        "<div class='card-body'>" +
        "<h4 class='card-title'>" +
        "<a href='product#" + product.id + "'>" +
        product.title +
        "</a>" +
        "</h4>" +
        "<h5>" +
        product.price +
        "€</h5>" +
        "<p class='card-text text-truncate'>" +
        product.description +
        "</p>" +
        "</div>" +
        "<div class='card-footer' style='display: grid;'>" +
        "<span class='badge badge-dark' style='max-width: max-content;'>" + product.category + "</span>" +
        "<button class='btn btn-outline-success' style='' type='button'" +
        " onClick='setProductInCart(" + product.id + ")'>" +
        "Ajouter au panier" +
        "</button>" +
        "</div></div>"
    );
  });

  categoryListHtml = [...new Set(categoryListHtml)];
  document.getElementById("categoryList").innerHTML = categoryListHtml.join("");
  document.getElementById("cardSection").innerHTML = productListHtml.join("");
  document.querySelectorAll('.loading').forEach(function(loading){
    loading.remove()
  })
}

if (sessionStorage.getItem("cart")) {
  sessionList = JSON.parse(sessionStorage.getItem("cart"));
  sessionList.forEach((product, index) => {
    setProductInCart(product.id)
  })
}

function setProductInCart(selectedProduct) {
  listProductId.push(parseInt(selectedProduct));
}

function openCart() {
  var filterById;
  cartListHtml = []

  listProductId.forEach((product, index) => {
    filterById = products.filter((item) => item.id === product)[0];
    if (filterById !== cartList[index]) {
      cartList.push(filterById);
    }
  });

  cartList.forEach((list, index) => {
    cartListHtml.push("<li>" + list.title + "</li>");
  });

  document.getElementById("cartSection").innerHTML = cartListHtml.join("");
}

function accessPayment(item, array, page) {
  if (array.length !== 0) {
    sessionStorage.setItem(item, JSON.stringify(array));
    window.location.href = page;
  }
}

function accessPage(item, array, elmtId) {
  document.getElementById(elmtId).disabled = array.length === 0;
}

// je génére la liste des category en HTML avec en parametre le nom de la categorie
function generateProductCategory(product) {
  categoryListHtml.push(
      '<a href="#" onclick="getCategory(\'' + product.category + '\')" class="list-group-item">' + product.category + '</a>'
  )
}

// au clic sur une categorie, je récupére le nom de cette categorie et je fais un appel HTTP vers cette categorie
function getCategory(categoryName) {
  fetch("https://fakestoreapi.com/products/category/" + categoryName)
    .then((res) => res.json())
    .then((json) => {
      products = json;
      // je génere la liste des cartes en HTML avec ma nouvelle liste de produit
      generateProductsCard(products);
    });
}

// filtre les produits par ordre croissant
function filterBy(param) {
  var filterTable = products;
  if (priceTable.length > 0) {
    filterTable = priceTable;
  }
  if (param === "asc") {
    filterTable.sort((a, b) => {
      return a.price - b.price;
    });
  }
  if (param === "desc") {
    filterTable.sort((a, b) => {
      return b.price - a.price;
    });
  }
  generateProductsCard(filterTable);
}

// filtre les produits par prix min et max
function filterByPrice() {
  priceTable = [];
  // products = liste des produits
  // product = chaque produit
  var priceMin = document.getElementById("prixMin").value;
  var priceMax = document.getElementById("prixMax").value;

  if (priceMin > priceMax) {
    alert("Le prix minimal ne peut pas etre superieur au maximal");
    document.getElementById("prixMin").value = "";
    document.getElementById("prixMax").value = "";
    return;
  }

  products.forEach((product, index) => {
    if (product.price >= priceMin && product.price <= priceMax) {
      priceTable.push(product);
    }
  });
  generateProductsCard(priceTable);
}

var balise, i, file, xhttp;
/* Parcours tous les éléments HTML: */
balise = document.getElementsByTagName("*");
for (i = 0; i < balise.length; i++) {
  includeHTML(balise[i])
}

function includeHTML(include) {

  if (include) {
    /* Recherche les éléments avec l'attribut include: */
    file = include.getAttribute("include");
  }
    if (file) {
      /* Requête HTTP utilisant la valeur d'attribut comme nom de fichier: */
      xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (this.readyState === 4) {
          if (this.status === 200) {include.innerHTML = this.responseText;}
          if (this.status === 404) {include.innerHTML = "Page not found.";}
          /* Remove the attribute, and call this function once more: */
          include.removeAttribute("include");
          includeHTML();
        }
      }
      xhttp.open("GET", file, true);
      xhttp.send();
    }
}

getProducts();
