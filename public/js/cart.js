var cart = [];
var cartHtml = [];

function getCart() {
  cart = sessionStorage.getItem("cart");
  cart = JSON.parse(cart);
  generateCartHtml(cart);
}

function generateCartHtml(cart) {
  cartHtml = [];
  cart.forEach((product, index) => {
    cartHtml.push(
      "<li class='list-group-item'>" +
        "<a href='product.html#" + product.id + "'>" +
        product.title +
        "</a>" +
        "<hr>" +
        "<p>" +
        product.description +
        "</p>" +
        "<hr>" +
        "<h5>" +
        product.price +
        "€</h5>" +
        "<br>" +
        '<button class="btn btn-danger" type="button" onClick="deleteProduct(\'' +
        index +
        "')\">" +
        "Supprimer" +
        "</button>" +
        "</li>"
    );
  });
  document.getElementById("cartSection").innerHTML = cartHtml.join("");
}

function returnToList() {
  return (window.location.href = "index.html");
}

function deleteProduct(index) {
  console.log(cart)
  delete cart[index];
  cart = cart.filter((elm) => elm);
  sessionStorage.setItem("cart", JSON.stringify(cart));
  generateCartHtml(cart);
  if (cart.length === 0) {
    sessionStorage.removeItem("cart");
  }
}

getCart();
