var path = window.location.hash.substr(1);
var product = false;

function ifNoId() {
    if (window.location.href === window.location.href.replace(window.location.hash, '')) {
        window.location.replace("http://localhost/cesi-ecommerce/public/");
    }
}
ifNoId()


// récupére le produit en API
function getProduct() {
    fetch("https://fakestoreapi.com/products/"+path)
        .then((res) => res.json())
        .then((json) => {
            product = json;
            generateProductCard(product);
        });
}

// génére la card produit en HTML
function generateProductCard(product) {
    document.getElementById("container").innerHTML =
            "<div class='row'>" +
            "<div class='col-md-4'>" +
            "<img class='w-100'" +
            " src='" + product.image + "' alt='" + product.title + "'>" +
            "</div>" +
            "<div class='col-md-8'>" +
            "<h4 class='text-primary'>" +
            product.title +
            "</h4>" +
            "<h5>" +
            product.price +
            "€</h5>" +
            "<p>" +
            product.description +
            "</p>" +
            "</div></div>" +
            "<div class='d-flex justify-content-between align-items-center'>" +
            "<span class='badge badge-dark'>" + product.category + "</span>" +
            "<button class='btn btn-outline-success' style='' type='button'" +
            " onClick='setProductInCart(" + product.id + ")'>" +
            "Ajouter au panier" +
            "</button>" +
            "</div>"
    ;
}

getProduct()